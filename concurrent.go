package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"sync"
	"time"
)

func parallelSort(wg *sync.WaitGroup, arr []int, ch chan []int) {
	defer wg.Done()

	sort.Ints(arr)

	ch <- arr
}

func mergeSortedArrays(sortedArrays [][]int) []int {
	var result []int

	for _, arr := range sortedArrays {
		result = mergeSorted(result, arr)
	}

	return result
}

func mergeSorted(arr1, arr2 []int) []int {
	result := make([]int, 0, len(arr1)+len(arr2))
	i, j := 0, 0

	for i < len(arr1) && j < len(arr2) {
		if arr1[i] < arr2[j] {
			result = append(result, arr1[i])
			i++
		} else {
			result = append(result, arr2[j])
			j++
		}
	}

	result = append(result, arr1[i:]...)
	result = append(result, arr2[j:]...)

	return result
}

func main() {

	to_sort := `[
        3, 2,
    ]`

	var arr []int
	if err := json.Unmarshal([]byte(to_sort), &arr); err != nil {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	numGoroutines := 4

	subarraySize := len(arr) / numGoroutines

	ch := make(chan []int, numGoroutines)

	var wg sync.WaitGroup

	for i := 0; i < numGoroutines; i++ {
		wg.Add(1)

		start := i * subarraySize
		end := (i + 1) * subarraySize

		if i == numGoroutines-1 {
			end = len(arr)
		}

		subarray := arr[start:end]

		go parallelSort(&wg, subarray, ch)
	}

	wg.Wait()

	close(ch)

	var sortedArrays [][]int
	for sortedArray := range ch {
		sortedArrays = append(sortedArrays, sortedArray)
	}

	sortedResult := mergeSortedArrays(sortedArrays)

	sortedJSON, err := json.Marshal(sortedResult)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}

	fmt.Println("Sorted Array:", string(sortedJSON))
	fmt.Println(string(sortedJSON))
	end_time := time.Since(start_time)
	elapsed_time := end_time.Nanoseconds()
	fmt.Printf("Program execution time: %dns\n", elapsed_time)
	handler := http.HandlerFunc(handleRequest)
	http.Handle("/process-concurrent", handler)
	http.ListenAndServe(":8000", nil)
}
func handleRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)

	resp["sorted_arrays"] = "[[sorted_sub_array1],[sorted_sub_array2],[sorted_sub_array3]]"

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	w.Write(jsonResp)
	return
}
