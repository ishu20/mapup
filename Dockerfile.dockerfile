
FROM golang:1.19.2-bullseye



 

WORKDIR /sequential.go
 

COPY . ./C:\Users\ksisw\OneDrive\Documents\getting-started-app\sequential.go
 

RUN go run sequential.go
 

RUN go build -o /sequential.go
 
EXPOSE 8000
 
CMD [ “/sequential.go” ]