package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"time"
)

func main() {
	start_time := time.Now()
	to_sort := `[
        [3, 2, 1],
        [6, 5, 4],
        [9, 8, 7]
    ]`

	var a [][]int
	if err := json.Unmarshal([]byte(to_sort), &a); err != nil {
		panic(err)
	}

	for _, subArr := range a {
		sort.Slice(subArr, func(i, j int) bool {
			return subArr[i] < subArr[j]
		})
	}

	sortedJSON, err := json.Marshal(a)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(sortedJSON))
	end_time := time.Since(start_time)
	elapsed_time := end_time.Nanoseconds()
	fmt.Printf("Program execution time: %dns\n", elapsed_time)
	handler := http.HandlerFunc(handleRequest)
	http.Handle("/process-single", handler)
	http.ListenAndServe(":8000", nil)
}
func handleRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)

	resp["sorted_arrays"] = "[[sorted_sub_array1],[sorted_sub_array2],[sorted_sub_array3]]"

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	w.Write(jsonResp)
	return
}
